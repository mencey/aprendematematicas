-- phpMyAdmin SQL Dump
-- version 3.3.2deb1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 09-08-2010 a las 17:03:13
-- Versión del servidor: 5.1.41
-- Versión de PHP: 5.3.2-1ubuntu4.2

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `financiero`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `acerca`
--

CREATE TABLE IF NOT EXISTS `acerca` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `texto` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Volcar la base de datos para la tabla `acerca`
--

INSERT INTO `acerca` (`id`, `texto`) VALUES
(1, '<h1>Acerca de esta apliacci&oacute;n</h1>\r\n<p>Introduce el resultado, por ejemplo para hallar la suma de 4 y 5, todo dividido por 2, introducir: (4+5)/2</p>\r\n\r\n<hr/>\r\n<p>Aprende Matem&aacute;ticas, fue desarrollado por <a href="http://www.eduardonacimiento.com">Eduardo Nacimiento Garc&iacute;a</a>. Se encuentra licenciado bajo licencia <a href="http://www.gnu.org/licenses/agpl-3.0.html">GNU AGPL</a> y se puede <a href="http://gitorious.org/mencey/aprendematematicas">descargar libremente desde internet</a></p>\r\n\r\n<p>Se hace uso de <a href="http://www.gnu.org/software/bc/">bc - An arbitrary precision calculator language</a> y de <a href="http://mathcs.chapman.edu/~jipsen/mathml/asciimath.html">ASCIIMathML</a>.</p>\r\n');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ayuda`
--

CREATE TABLE IF NOT EXISTS `ayuda` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `texto` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Volcar la base de datos para la tabla `ayuda`
--

INSERT INTO `ayuda` (`id`, `texto`) VALUES
(1, '<h1>Ayuda</h1>\r\n<p>Esta es la ayuda para el tema 1 de Financiero</p>');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `datos`
--

CREATE TABLE IF NOT EXISTS `datos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idpregunta` int(11) NOT NULL,
  `idexamen` int(11) NOT NULL,
  `datos` longtext NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idpregunta` (`idpregunta`,`idexamen`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Volcar la base de datos para la tabla `datos`
--

INSERT INTO `datos` (`id`, `idpregunta`, `idexamen`, `datos`) VALUES
(1, 1, 1, '40000|5000|0.12|10'),
(2, 2, 1, '230000|100000|6|0.10');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `examen`
--

CREATE TABLE IF NOT EXISTS `examen` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` text,
  `resumen` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Volcar la base de datos para la tabla `examen`
--

INSERT INTO `examen` (`id`, `descripcion`, `resumen`) VALUES
(1, '<p>Examen de Analisis de los Instrumentos financieros</p>\r\n<p>Tema 1</p>', 'Financiero1'),
(2, '<p>Examen de Analisis de los Instrumentos financieros</p>\r\n<p>Tema 2</p>', 'Financiero2');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `preguntas`
--

CREATE TABLE IF NOT EXISTS `preguntas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `examen` int(11) NOT NULL,
  `texto` longtext CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL,
  `formula` text CHARACTER SET latin1 COLLATE latin1_spanish_ci,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Volcar la base de datos para la tabla `preguntas`
--

INSERT INTO `preguntas` (`id`, `examen`, `texto`, `formula`) VALUES
(1, 1, 'Cual es el valor inicial de una apartamento por el que se pide: #0#  al contado y #3# anualidades de #1# cada una, si consideramos el interes del #2# efectivo anual?\r\n', '#0#+#1#*(1-(1+#2#)^(-#3#))/#2#'),
(2, 1, 'Una empresa compra unos terrenos por valor de #0#, pagando #1# al contado y comprometiendose a pagar el resto en #2# pagos anuales iguales. Cual deberia ser el importe anual de cada pago si el tipo de interes es el #3# TAE?\r\n', '((#0#-#1#)*#3)/(1-(1+#3#)^(-#2#))');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `respuestas`
--

CREATE TABLE IF NOT EXISTS `respuestas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(12) NOT NULL,
  `idpregunta` int(11) NOT NULL,
  `idexamen` int(11) NOT NULL,
  `datos` longtext NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user` (`user`,`idpregunta`,`idexamen`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Volcar la base de datos para la tabla `respuestas`
--

INSERT INTO `respuestas` (`id`, `user`, `idpregunta`, `idexamen`, `datos`) VALUES
(5, 'mencey', 1, 1, '40000+5000*(1-(1+0.12)^(-10))/0.12'),
(6, 'mencey', 2, 1, '4+5');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `test`
--

CREATE TABLE IF NOT EXISTS `test` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `examen` int(11) NOT NULL,
  `user` varchar(12) NOT NULL,
  `nota` float DEFAULT NULL,
  `fechaini` int(11) DEFAULT NULL,
  `fechafin` int(11) DEFAULT NULL,
  `secuencia` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `examen` (`examen`,`user`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Volcar la base de datos para la tabla `test`
--


-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(12) NOT NULL,
  `password` text NOT NULL,
  `rol` text,
  `nombre` text,
  `correo` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user` (`user`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Volcar la base de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `user`, `password`, `rol`, `nombre`, `correo`) VALUES
(2, 'prueba', 'prueba', 'registrado', 'Usuario de Pruebas', 'tucorreo@eduardonacimiento.com');
